﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KomarenkoCwaniak
{
    public abstract class Person
    {
        public abstract void AbstractMethod();
        public abstract void AbstractMethod(string message);

        public virtual void AbstractMethod(string message, int count)
        {
            Console.WriteLine("First Implementation");
        }
    }
}
