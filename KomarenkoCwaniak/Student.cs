﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KomarenkoCwaniak
{
    class Student : Person
    {
        public string FirstName { get; set; }
        public void GoHome(School school)
        {
            AbstractMethod("", 5);

            //Console.WriteLine($"I'm going home {FirstName} called in class {typeof(Student)} send from {school}");
        }

        public string SomethingForSchool { get; set; }

        public void SendMessage()
        {
        }

        //public override void AbstractMethod(string message, int count)
        //{
        //    Console.WriteLine("Another implementation");
        //}

        public void SendMessage(School randomSchool)
        {
            base.AbstractMethod("", 5);
            //randomSchool.ReceiveMessage();
            //SomethingForSchool = "ChangeValue";
            //StudentFound?.Invoke();
        }

        public override void AbstractMethod()
        {
            throw new NotImplementedException();
        }

        public override void AbstractMethod(string message)
        {
            throw new NotImplementedException();
        }

        public event Action StudentFound = delegate { };
    }
}
