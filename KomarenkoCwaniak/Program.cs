﻿using System;
using System.Collections.Generic;

namespace KomarenkoCwaniak
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var school = new School(new List<Student>
                {
                    new Student(),
                    new Student(),
                    new Student(),
                    new Student(),
                });
            school.ReceiveMessage();
            school.SendStudentHome();
            
        }
    }
}
