﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KomarenkoCwaniak
{
    class School
    {
        public School(IEnumerable<Student> students)
        {
            Students = students;
        }

        private IEnumerable<Student> Students { get; set; } = new List<Student>();

        public void SendStudentHome()
        {
            foreach (var student in Students)
            {
                student.StudentFound += OtherName;
                student.SendMessage();
                student.GoHome(this);
            }
        }

        private void OtherName()
        {
            Console.WriteLine($"I got a message from Student");
        }

        public void ReceiveMessage()
        {
            Console.WriteLine($"Message from student called from class { typeof(School)} called from method {nameof (ReceiveMessage)}" );
        }

    }
}
